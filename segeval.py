# -*- encoding:utf-8 -*-
import difflib


def segeval(fname_dict, fname_gold, fname_result):
    fg = open(fname_gold)
    fj = open(fname_result)
    fd = open(fname_dict)

    lineg = fg.readlines()
    linej = fj.readlines()
    lined = fd.readlines()
    fg.close()
    fj.close()
    fd.close()

    mdict = set([x.strip().decode("utf-8") for x in lined])
    i = 0
    count_g = 0
    count_j = 0
    count_match = 0
    iv_all = 0
    iv_correct = 0
    oov_all = 0
    oov_correct = 0

    for lines in zip(lineg, linej):
        print i
        l0 = lines[0].decode('utf-8').strip().split("  ")
        l1 = lines[1].decode('utf-8').strip().split("  ")
        #print "|".join(l0)
        #print "|".join(l1)

        s = difflib.SequenceMatcher(None, l0, l1)
        match_block = s.get_matching_blocks()
        match_range = reduce(lambda a, b: a + b, [range(x[0], x[0] + x[2]) for x in match_block])

        c_g = len(l0)
        c_j = len(l1)
        c_m = sum(x[2] for x in match_block)
        # print c_g, c_j, c_m
        # print c_m / float(c_g)
        # print c_m / float(c_j)
        for idx, w in enumerate(l0):
            if w in mdict:
                iv_all += 1
                if idx in match_range:
                    iv_correct += 1
            else:
                oov_all += 1
                if idx in match_range:
                    oov_correct += 1
        # print match_range
        # print iv_all, iv_correct
        # print oov_all, oov_correct

        count_g += c_g
        count_j += c_j
        count_match += c_m
        i += 1

        # if i>5:
        #    break

    m_p = count_match / float(count_g)
    m_r = count_match / float(count_j)
    m_f = 2 * m_p * m_r / (m_p + m_r)
    m_oov_r = oov_all / float(iv_all + oov_all)
    m_iv = iv_correct / float(iv_all)
    m_oov = oov_correct / float(oov_all)
    print "-----------"
    print "precision:%s" % (m_p)
    print "recall:%s" % (m_r)
    print "f-measure:%s" % (m_f)
    print "oov-rate:%s" % (m_oov_r)
    print "oov-recall:%s" % (m_oov)
    print "iv-recall:%s" % (m_iv)
    print "-----------"


def main():
    fname_gold = "data/icwb2-data/gold/pku_test_gold.utf8"
    fname_result = "result/pku_test_out.utf8"

    #fname_gold = "data/icwb2-data/training/pku_training.utf8"
    #fname_result = "result/pku_train_out.utf8"
    fname_dict = "data/icwb2-data/gold/pku_training_words.utf8"

    segeval(fname_dict, fname_gold, fname_result)

if __name__ =="__main__":
    main()
