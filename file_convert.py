import theano.tensor as T
import theano
import numpy as np
import json
import math
from collections import OrderedDict
import random
from util import read_file_line, read_json, tagDigitEn
import os.path
random.seed(1)


# U 0 unigram
# B 1 begin
# L 2 last
# I 3 intermediate

def get_widx(w, word_dict):
    w_idx = word_dict.get(tagDigitEn(w), None)
    if w_idx:
        return w_idx
    else:
        return word_dict.get("OOV")


def gen_word_idx(line, word_dict):
    line_write = []
    line2 = filter(lambda x: len(x) > 0, [w.strip() for w in line])
    if len("".join(line2)) > 1:
        for w in line2:
            w_idx = get_widx(w, word_dict)
            line_write.append(w_idx)
    return line_write


def gen_word_label(line, word_dict):
    line_write = []
    line2 = filter(lambda x: len(x) > 0, [w.strip() for w in line.split(" ")])
    if len("".join(line2)) > 1:
        for ws in line2:
            if len(ws) == 1:
                line_write.append("%s,%s" % (get_widx(ws, word_dict), 0))
            else:
                for i, w in enumerate(ws):
                    if i == 0:
                        line_write.append("%s,%s" % (get_widx(w, word_dict), 1))
                    elif i == len(ws) - 1:
                        line_write.append("%s,%s" % (get_widx(w, word_dict), 2))
                    else:
                        line_write.append("%s,%s" % (get_widx(w, word_dict), 3))
    return line_write


def gen_training_file_label(train_file, train_file_idx, word_dict):
    # f = open(train_file,"r")
    f2 = open(train_file_idx, "w")
    for line in read_file_line(train_file, True):
        line_write = gen_word_label(line.decode("utf-8"), word_dict)
        if len(line_write) > 1:
            f2.write("%s\n" % (" ".join([str(x) for x in line_write])))
    f2.close()


def gen_training_file_idx(train_file, train_file_idx, word_dict):
    # f = open(train_file,"r")
    f2 = open(train_file_idx, "w")
    for line in read_file_line(train_file, True):
        line_write = gen_word_idx(line.decode("utf-8"), word_dict)
        if len(line_write) > 1:
            f2.write("%s\n" % (" ".join([str(x) for x in line_write])))
    f2.close()


def gen_train_val_file_label_idx(ifile, path, fid, word_dict, sep=0.9):
    # f = open(train_file,"r")
    fpath = os.path.join(path,fid)
    f_t_label = open(fpath + "_t_label.txt", "w")
    f_t_idx = open(fpath + "_t_idx.txt", "w")
    f_v_label = open(fpath + "_v_label.txt", "w")
    f_v_idx = open(fpath + "_v_idx.txt", "w")
    for line in read_file_line(ifile, True):
        line_write_label = gen_word_label(line.decode("utf-8"), word_dict)
        line_write_idx = gen_word_idx(line.decode("utf-8"), word_dict)
        if len(line_write_label) > 1 and len(line_write_idx) > 1:
            if random.random() > sep:
                f_v_label.write("%s\n" % (" ".join([str(x) for x in line_write_label])))
                f_v_idx.write("%s\n" % (" ".join([str(x) for x in line_write_idx])))
            else:
                f_t_label.write("%s\n" % (" ".join([str(x) for x in line_write_label])))
                f_t_idx.write("%s\n" % (" ".join([str(x) for x in line_write_idx])))
    f_t_label.close()
    f_t_idx.close()
    f_v_label.close()
    f_v_idx.close()



def main():
    dict_fname = "word_idx_dict.json"
    fout_path1 = "pku_data_idx.txt"
    fout_path2 = "pku_data_label_idx.txt"
    word_dict = read_json(dict_fname)
    #fpath = "./data/icwb2-data/training/pku_training.utf8"
    fpath = "./data/icwb2-data/gold/pku_test_gold.utf8"
    # gen_training_file_idx(fpath, fout_path1, word_dict)
    #gen_training_file_label(fpath, fout_path2, word_dict)
    fout_path = "./data/full"
    fout_id = "pku_test"
    gen_train_val_file_label_idx(fpath,fout_path,fout_id,word_dict,1)


if __name__ == "__main__":
    main()
