# -*- encoding:utf-8 -*-
from collections import Counter, OrderedDict
import json
import re

def tagDigitEn(w):
    if w.isdigit():
        return u"$NUM$"
    elif len(re.findall(ur"[A-Za-z]",w)) > 0:
        return u"$EN$"
    else:
        return w



vocab_size = 4000
splitter = u"  "
fpath = "./data/icwb2-data/training/pku_training.utf8"
f = open(fpath,"r")
wcount = Counter()
i = 0
while True:
    line = f.readline()
    i += 1
    #if i == 100:
    #    break
    print i
    if len(line) == 0:
        break
    else:
        line2 = filter(lambda x: len(x) > 0, [tagDigitEn(w.strip()) for w in line.decode("utf-8")])
        #print ",".join(line2)
        wcount.update(line2)
f.close()
f2 = open("word_freq_dict.json","w")
f3 = open("word_idx_dict.json","w")

word_freq_dict =OrderedDict(wcount.most_common(vocab_size-2))
word_freq_dict.update({"OOV":1})
word_freq_dict.update({"PAD":1})
word_idx_dict = OrderedDict([(x[1],x[0]) for x in enumerate(word_freq_dict.keys())])
f2.write(json.dumps(word_freq_dict, indent=4))
f2.close()
f3.write(json.dumps(word_idx_dict, indent=4))
f3.close()

