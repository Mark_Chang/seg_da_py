import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json
import logging
from profiler import Profiler, AvgCounter

np.random.seed(1)

logging.basicConfig(filename='out.log', format='%(asctime)s [%(levelname)s]:%(message)s', level=logging.INFO,
                    datefmt='%Y-%m-%d %I:%M:%S')

negsamp_array = []


def load_negsamp_array(word_dict):
    negsamp_array = []

    word_dict_list = sorted(word_dict.values(), reverse=True)
    word_dict_list = [w for w in enumerate(word_dict_list)]

    negsamp_max_count = max([x[1] for x in word_dict_list])
    # word_dict_list.append((len(word_dict_list),negsamp_max_count-2))
    word_dict_list[-2] = (len(word_dict_list) - 2, negsamp_max_count)
    word_dict_list[-1] = (len(word_dict_list) - 1, negsamp_max_count / 10)

    word_dict_list = [(x[0], int(math.log(x[1], 2) + 1)) for x in word_dict_list]

    negsamp_array_size = sum([x[1] for x in word_dict_list])
    negsamp_array = np.zeros((negsamp_array_size)).astype(int)
    offset = 0
    for witem in word_dict_list:
        negsamp_array[offset:offset + witem[1]] = witem[0]
        offset += witem[1]
    return negsamp_array


def my_print(s):
    print s
    logging.info(s)


def gen_neg_idx(target, negsamp_array):
    assert len(negsamp_array > 0)
    while True:
        #result = #negsamp_array[int(math.floor(np.random.rand() * len(negsamp_array)))]
        result = int(math.floor(np.random.rand() * (negsamp_array)))
        if result != target:
            break
    return result


def weight_init(shape):
    return theano.shared(
        ((0.5 - np.random.rand(*shape)) * (np.sqrt(6) / np.sqrt(sum(shape)))).astype(theano.config.floatX))


def weight_init_val(w_val):
    return theano.shared(np.array(w_val).astype(theano.config.floatX))


def weight_init_const(shape, const):
    return theano.shared((const * np.ones(shape)).astype(theano.config.floatX))


def adagrad_update(parameters, gradients, eta, eps, init_params=None):
    # create variables to store intermediate updates
    gradients_sq_dict = OrderedDict()
    deltas_sq_dict = OrderedDict()
    for pkey in parameters.keys():
        pval = parameters[pkey]
        if init_params:
            temp_grad = weight_init_val(np.array(init_params['gradients_sq'][pkey]))
            # temp_delt = np.array(init_params['deltas_sq'][pkey])
        else:
            temp_grad = weight_init_const(pval.get_value().shape, eps)
            # temp_delt = np.zeros(pval.get_value().shape)

        gradients_sq_dict.update({pkey: temp_grad})
        # deltas_sq_dict.update({pkey: weight_init_val(temp_delt)})
    # calculates the new "average" delta for the next iteration
    gradients_sq_new = [g_sq + (g ** 2) for g_sq, g in zip(gradients_sq_dict.values(), gradients)]

    # calculates the step in direction. The square root is an approximation to getting the RMS for the average value
    deltas = [(eta / T.sqrt(g_sq + eps)) * grad for g_sq, grad in zip(gradients_sq_new, gradients)]

    # Prepare it as a list f
    gradient_sq_updates = zip(gradients_sq_dict.values(), gradients_sq_new)
    # deltas_sq_updates = zip(deltas_sq_dict.values(), deltas_sq_new)
    parameters_updates = [(p, p - d) for p, d in zip(parameters.values(), deltas)]
    return gradient_sq_updates + parameters_updates, gradients_sq_dict, deltas_sq_dict


def adadelta_updates(parameters, gradients, rho, eps, init_params=None):
    # create variables to store intermediate updates
    gradients_sq_dict = OrderedDict()
    deltas_sq_dict = OrderedDict()
    for pkey in parameters.keys():
        pval = parameters[pkey]
        if init_params:
            temp_grad = np.array(init_params['gradients_sq'][pkey])
            temp_delt = np.array(init_params['deltas_sq'][pkey])
        else:
            temp_grad = np.zeros(pval.get_value().shape)
            temp_delt = np.zeros(pval.get_value().shape)

        gradients_sq_dict.update({pkey: weight_init_val(temp_grad)})
        deltas_sq_dict.update({pkey: weight_init_val(temp_delt)})
    # calculates the new "average" delta for the next iteration
    gradients_sq_new = [rho * g_sq + (1 - rho) * (g ** 2) for g_sq, g in zip(gradients_sq_dict.values(), gradients)]

    # calculates the step in direction. The square root is an approximation to getting the RMS for the average value
    deltas = [(T.sqrt(d_sq + eps) / T.sqrt(g_sq + eps)) * grad for d_sq, g_sq, grad in
              zip(deltas_sq_dict.values(), gradients_sq_new, gradients)]
    # calculates the new "average" deltas for the next step.
    deltas_sq_new = [rho * d_sq + (1 - rho) * (d ** 2) for d_sq, d in zip(deltas_sq_dict.values(), deltas)]

    # Prepare it as a list f
    gradient_sq_updates = zip(gradients_sq_dict.values(), gradients_sq_new)
    deltas_sq_updates = zip(deltas_sq_dict.values(), deltas_sq_new)
    parameters_updates = [(p, p - d) for p, d in zip(parameters.values(), deltas)]
    return gradient_sq_updates + deltas_sq_updates + parameters_updates, gradients_sq_dict, deltas_sq_dict


def build_networks(config, init_params=None):
    s_vocab = config["s_vocab"]
    s_embed = config["s_embed"]
    s_window = config["s_window"]
    s_hidden = config["s_hidden"]
    p_lambda = config.get("p_lambda", 10.)
    p_rho = 0.9
    p_eta = 0.1
    #p_eps = 0.1
    p_eps = 1.0

    wb = OrderedDict()
    # gdss = {}
    # deltass = {}
    if init_params:
        for wb_s_key in init_params['wb'].keys():
            wb[wb_s_key] = weight_init_val(init_params['wb'][wb_s_key])
    else:
        wb_shape = OrderedDict({
            "w_embed": (s_vocab, s_embed),
            "w_hidden": (s_embed * s_window, s_hidden),
            "b_hidden": (s_hidden,),
            "w_out": (s_hidden, 1),
            "b_out": (1,)
        })
        for wb_s_key in wb_shape.keys():
            wb[wb_s_key] = weight_init(wb_shape[wb_s_key])

    x_in = T.imatrix()
    y_in = T.imatrix()

    emb_lookup = wb["w_embed"][x_in]
    hidden_input = T.reshape(emb_lookup, newshape=(x_in.shape[0], x_in.shape[1] * s_embed))
    hidden_result = T.tanh(T.dot(hidden_input, wb["w_hidden"]) + wb["b_hidden"])
    out_result = T.nnet.sigmoid(T.dot(hidden_result, wb["w_out"]) + wb["b_out"])

    w_array = [T.sum(T.power(wb[k], 2)) for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    w_array_size = [wb[k].shape[0]*wb[k].shape[1] for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    w_sum = sum(w_array)/T.cast(sum(w_array_size),theano.config.floatX )

    error_cost = - T.mean((y_in * T.log(out_result) + (1 - y_in) * T.log(1 - out_result)))
    cost = error_cost + p_lambda * w_sum

    gd = OrderedDict()
    for wb_key in wb.keys():
        gd[wb_key] = T.grad(cost=cost, wrt=wb[wb_key])

    #wb_updates, gradients_sq, deltas_sq = adadelta_updates(wb, gd.values(), p_rho, p_eps, init_params)
    wb_updates, gradients_sq, deltas_sq = adagrad_update(wb, gd.values(), p_eta, p_eps, init_params)

    # func = theano.function(inputs=[x_in, y_in], outputs=[emb_lookup, hidden_input, hidden_result, out_result, cost],
    #                       updates=wb_updates, allow_input_downcast=True)

    t_func = theano.function(inputs=[x_in, y_in], outputs=[out_result, cost, error_cost, w_sum], updates=wb_updates,
                             allow_input_downcast=True)

    v_func = theano.function(inputs=[x_in, y_in], outputs=[out_result, cost], allow_input_downcast=True)

    params = OrderedDict()
    params['wb'] = wb
    params['gradients_sq'] = gradients_sq
    params['deltas_sq'] = deltas_sq
    return t_func, v_func, params


def write_model(params, fname, f_indent=False):
    params_json = {}
    p_keys = ['wb', 'gradients_sq', 'deltas_sq']
    for p_key in p_keys:
        params_json[p_key] = {}
        for w_key in params[p_key]:
            params_json[p_key][w_key] = params[p_key][w_key].get_value().tolist()
    params_json['params'] = {}
    if params.get('params'):
        params_json['params'] = json.dumps(params['params'])
    f = open(fname, "w")
    if f_indent:
        f.write(json.dumps(params_json, indent=4))
    else:
        f.write(json.dumps(params_json))
    f.close()


def gen_input_line(line, s_window, pad_id, negsamp_array):
    line_word = [pad_id] * (s_window / 2) + [int(w) for w in line.strip().split(" ")] + [pad_id] * (
        s_window / 2)
    case_raw = [line_word[i:i + s_window] for i in range(len(line_word) + 1 - s_window)]
    case_positive_raw = zip(case_raw, [1] * len(case_raw))
    case_negative_raw = zip(
        [copy.deepcopy(item[:s_window / 2]) + [gen_neg_idx(item[s_window / 2], negsamp_array)] + copy.deepcopy(
            item[s_window / 2 + 1:])
         for item in case_raw], [0] * len(case_raw))
    case_all = case_positive_raw + case_negative_raw
    np.random.shuffle(case_all)
    [case_x, case_y] = zip(*case_all)
    case_x = list(case_x)
    case_y = list(case_y)
    return case_x, case_y


def read_input_file(train_file_idx, random_preload=True):
    # if random_preload:
    lines = [line for line in read_file_line(train_file_idx, False)]
    if random_preload:
        np.random.shuffle(lines)
    for line in lines:
        if len(line.strip()) > 0:
            yield line
            # else:
            #    for line in read_file_line(train_file_idx, False):
            #        if len(line.strip()) > 0:
            #            yield line


def gen_input_data(train_file_idx, s_window, pad_id, negsamp_array, random_preload=True):
    case_x_out = []
    case_y_out = []
    for line in read_input_file(train_file_idx, random_preload):
        case_x, case_y = gen_input_line(line, s_window, pad_id, negsamp_array)
        case_x_out.extend(case_x)
        case_y_out.extend(case_y)
        if len(case_x_out) > 8000:
            yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)
            case_x_out = []
            case_y_out = []
    msg = "remaining: %s " % (len(case_x_out))
    my_print(msg)
    if len(case_x_out) > 100:
        # yield np.array(case_x_out), np.array(case_y_out)
        yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)


def reset_sq(params):
    p_eps = 0.1
    p_keys = ['gradients_sq', 'deltas_sq']
    for p_key in p_keys:
        for w_key in params[p_key]:
            temp_shape = params[p_key][w_key].get_value().shape
            params[p_key][w_key].set_value((p_eps * np.ones(temp_shape)).astype(theano.config.floatX))


def get_correct_ratio(golden, predicted):
    assert len(golden) == len(predicted)
    return np.sum(golden == (predicted > 0.5).astype(int)) / float(len(golden))


def trainig(t_file_idx, v_file_idx, t_networks, v_networks, params, nconfig, training_config):
    s_window = nconfig["s_window"]
    pad_id = nconfig["s_vocab"] - 1



    tprofiler = Profiler("train")
    avg_t_cost = AvgCounter("cost")
    avg_t_correct = AvgCounter("correct")
    avg_t_total_correct = AvgCounter("avg_correct")

    avg_v_cost = AvgCounter("valid cost")
    avg_v_correct = AvgCounter("valid correct")

    avg_error_cost = AvgCounter("error cost")
    avg_reg_cost = AvgCounter("reg cost")

    for tid, tconfig in enumerate(training_config):
        #reset_sq(params)
        model_count = tconfig.get("model_offset", 0)
        print_time = tconfig.get("print_time", 500)

        negsamp_array = tconfig.get("negsamp",nconfig["s_vocab"])
        avg_t_cost.reset()
        avg_t_correct.reset()
        avg_t_total_correct.reset()
        avg_v_cost.reset()
        avg_t_correct.reset()

        avg_error_cost.reset()
        avg_reg_cost.reset()

        min_valid_cost = np.inf
        valid_wait = 0
        for iter in range(tconfig['iter']):
            tprofiler.start()

            j = 0
            # for line in read_input_file(train_file_idx, False):
            for case_x, case_y in gen_input_data(t_file_idx, s_window, pad_id, negsamp_array):

                result = t_networks(case_x, case_y)
                j += 1

                correct_ratio = get_correct_ratio(case_y, result[0])
                avg_t_cost.add(result[1])
                avg_t_correct.add(correct_ratio)
                avg_t_total_correct.add(correct_ratio)

                avg_error_cost.add(result[-2])
                avg_reg_cost.add(result[-1])

                if j % print_time == 0:
                    msg = "tid=%s iter=%s, j=%s, %s, %s " % (
                        tid, iter, j,
                        avg_t_cost.get_and_reset_str(),
                        avg_t_correct.get_and_reset_str()
                    )
                    my_print(msg)

                    msg2 = "%s, %s" % (
                        avg_error_cost.get_and_reset_str(),
                        avg_reg_cost.get_and_reset_str()
                    )
                    my_print(msg2)

            my_print("do validation...")

            for case_x, case_y in gen_input_data(v_file_idx, s_window, pad_id, negsamp_array, False):
                result = v_networks(case_x, case_y)
                cost = result[1]
                correct_ratio = get_correct_ratio(case_y, result[0])
                avg_v_cost.add(cost)
                avg_v_correct.add(correct_ratio)

            avg_v_cost_val = avg_v_cost.get_and_reset()
            avg_v_correct_val = avg_v_correct.get_and_reset()

            msg = "%s, %s" % (avg_t_total_correct.get_and_reset_str(), avg_v_correct_val)

            if avg_v_cost_val < min_valid_cost:
                valid_wait = 0
                min_valid_cost = avg_v_cost_val
                model_name = tconfig["model_name"] % model_count
                model_count += 1
                write_model(params, model_name)
                msg += ", save_model=%s" % (model_name)
            else:
                valid_wait += 1
                msg += ", valid_wait=%s" % (valid_wait)
                if valid_wait > 30:
                    msg += ", early stop"
                    my_print(msg)
                    break

            # print "save_model"
            # msg = "avg_correct=%s, save_model=%s" % (avg_t_correct.get_and_reset_str(), model_name)
            my_print(msg)
            tprofiler.stop()
            my_print(tprofiler.get_period())


def main():
    dict_fname = "word_freq_dict.json"
    word_dict = read_json(dict_fname)
    #negsamp_array = load_negsamp_array(word_dict)
    nconfig = {
        "s_vocab": len(word_dict),
        "s_embed": 100,
        "s_window": 11,
        "s_hidden": 300,
        "p_lambda": 0.00001,
    }
    t_idx_path = "./data/converted/pku_t_idx.txt"
    v_idx_path = "./data/converted/pku_v_idx.txt"

    training_config = [
        {"iter": 200,
         "model_name": "model_embed_1_%s.json",
         "print_time": 30,
         "negsamp":1000,
        #"model_offset":143,
         },
        {"iter": 200,
         "model_name": "model_embed_2_%s.json",
         "print_time": 30,
         "negsamp":2000,
         #"model_offset":143,
         },
        {"iter": 200,
         "model_name": "model_embed_3_%s.json",
         "print_time": 30,
         "negsamp":3000,
         #"model_offset":143,
         },
        {"iter": 200,
         "model_name": "model_embed_4_%s.json",
         "print_time": 30,
         "negsamp":4000,
         #"model_offset":143,
        #"model_offset":143,
         },
    ]
    #model_prarms = read_json("model_embed_142.json")
    t_networks, v_networks, params = build_networks(nconfig)#, init_params=model_prarms)
    trainig(t_idx_path, v_idx_path, t_networks, v_networks, params, nconfig, training_config)


if __name__ == "__main__":
    main()
