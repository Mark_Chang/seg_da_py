import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json
import logging
from profiler import Profiler, AvgCounter
from embedding_trainer import load_negsamp_array, my_print, weight_init, weight_init_val, weight_init_const, \
    adagrad_update, adadelta_updates, write_model, reset_sq, read_input_file

np.random.seed(1)

logging.basicConfig(filename='out.log', format='%(asctime)s [%(levelname)s]:%(message)s', level=logging.INFO,
                    datefmt='%Y-%m-%d %I:%M:%S')


def build_networks(config, pre_model_params, init_params=None):
    # s_vocab = config["s_vocab"]
    s_embed = config["s_embed"]
    s_window = config["s_window"]
    s_hidden = config["s_hidden"]
    s_hidden2 = config["s_hidden2"]
    s_hidden3 = config["s_hidden3"]
    p_lambda = config.get("p_lambda", 4.)
    s_out = 4
    p_rho = 0.9
    p_eta = 0.1
    p_eps = 0.1  # will be reset

    wb = OrderedDict()
    # gdss = {}
    # deltass = {}
    if init_params:
        for wb_s_key in init_params['wb'].keys():
            wb[wb_s_key] = weight_init_val(init_params['wb'][wb_s_key])
    else:
        wb['w_embed'] = weight_init_val(pre_model_params['wb']['w_embed'])
        wb['w_hidden'] = weight_init_val(pre_model_params['wb']['w_hidden'])
        wb['b_hidden'] = weight_init_val(pre_model_params['wb']['b_hidden'])
        wb['w_hidden2'] = weight_init_val(pre_model_params['wb']['w_hidden2'])
        wb['b_hidden2'] = weight_init_val(pre_model_params['wb']['b_hidden2'])
        wb['w_hidden3'] = weight_init_val(pre_model_params['wb']['w_hidden3'])
        wb['b_hidden3'] = weight_init_val(pre_model_params['wb']['b_hidden3'])
        wb_shape = OrderedDict({
            # "w_embed": (s_vocab, s_embed),
            # "w_hidden3": (s_hidden2, s_hidden3),
            # "b_hidden3": (s_hidden3,),
            "w_out": (s_hidden3, s_out),
            "b_out": (s_out,)
        })
        for wb_s_key in wb_shape.keys():
            wb[wb_s_key] = weight_init(wb_shape[wb_s_key])

    x_in = T.imatrix()
    y_in = T.imatrix()

    emb_lookup = wb["w_embed"][x_in]
    hidden_input = T.reshape(emb_lookup, newshape=(x_in.shape[0], x_in.shape[1] * s_embed))
    hidden_result = T.tanh(T.dot(hidden_input, wb["w_hidden"]) + wb["b_hidden"])
    hidden2_result = T.tanh(T.dot(hidden_result, wb["w_hidden2"]) + wb["b_hidden2"])
    hidden3_result = T.tanh(T.dot(hidden2_result, wb["w_hidden3"]) + wb["b_hidden3"])
    out_result = T.nnet.softmax(T.dot(hidden3_result, wb["w_out"]) + wb["b_out"])
    out_result_tag = T.argmax(out_result, axis=1)
    off = 1e-6

    w_array = [T.sum(T.power(wb[k], 2)) for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    w_array_size = [wb[k].shape[0] * wb[k].shape[1] for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    w_sum = sum(w_array) / T.cast(sum(w_array_size), theano.config.floatx)

    error_cost = -T.mean(T.log(out_result[T.arange(y_in.shape[0]), y_in[:, 0]] + off))
    cost = error_cost + p_lambda * w_sum

    gd = OrderedDict()
    for wb_key in wb.keys():
        gd[wb_key] = T.grad(cost=cost, wrt=wb[wb_key])

    #wb_updates, gradients_sq, deltas_sq = adadelta_updates(wb, gd.values(), p_rho, p_eps, init_params)
    wb_updates, gradients_sq, deltas_sq = adagrad_update(wb, gd.values(), p_eta, p_eps, init_params)

    # func = theano.function(inputs=[x_in, y_in], outputs=[emb_lookup, hidden_input, hidden_result, out_result, cost],
    #                      updates=wb_updates, allow_input_downcast=True)

    t_func = theano.function(inputs=[x_in, y_in], outputs=[out_result_tag, cost, error_cost, w_sum], updates=wb_updates,
                             allow_input_downcast=True)

    v_func = theano.function(inputs=[x_in, y_in], outputs=[out_result_tag, cost, error_cost, w_sum], 
                             allow_input_downcast=True)


    # func = theano.function(inputs=[x_in, y_in], outputs=[out_result], allow_input_downcast=True)
    # func = theano.function(inputs=[x_in], outputs=[out_result], allow_input_downcast=True)

    params = OrderedDict()
    params['wb'] = wb
    params['gradients_sq'] = gradients_sq
    params['deltas_sq'] = deltas_sq
    params['params'] = {'p_dropout': config.get("p_dropout", 1)}
    return t_func, v_func, params


def gen_input_line(line, s_window, pad_id, negsamp_array):
    line_word = [pad_id] * (s_window / 2) + [int(w.split(",")[0]) for w in line.strip().split(" ")] + [pad_id] * (
        s_window / 2)
    case_raw = [line_word[i:i + s_window] for i in range(len(line_word) + 1 - s_window)]
    line_tag = [int(w.split(",")[1]) for w in line.strip().split(" ")]

    case_all = zip(case_raw, line_tag)
    np.random.shuffle(case_all)
    [case_x, case_y] = zip(*case_all)
    return case_x, case_y


def gen_input_data(train_file_idx, s_window, pad_id, negsamp_array, random_preload=True):
    case_x_out = []
    case_y_out = []
    for line in read_input_file(train_file_idx, random_preload):
        case_x, case_y = gen_input_line(line, s_window, pad_id, negsamp_array)
        case_x_out.extend(case_x)
        case_y_out.extend(case_y)
        if len(case_x_out) > 8000:
            # yield np.array(case_x_out), np.array(case_y_out)
            yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)
            # print 1
            case_x_out = []
            case_y_out = []
    msg = "remaining: %s " % (len(case_x_out))
    my_print(msg)
    if len(case_x_out) > 100:
        # yield np.array(case_x_out), np.array(case_y_out)
        yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)


def get_correct_ratio(golden, predicted):
    assert len(golden) == len(predicted)
    return np.sum(golden[:, 0] == predicted) / float(len(golden))


def trainig(t_file_idx, v_file_idx, t_networks, v_networks, params, nconfig, training_config):
    s_window = nconfig["s_window"]
    pad_id = nconfig["s_vocab"] - 2

    tprofiler = Profiler("train")
    avg_t_cost = AvgCounter("cost")
    avg_t_correct = AvgCounter("correct")
    avg_t_total_correct = AvgCounter("avg_correct")

    avg_v_cost = AvgCounter("valid cost")
    avg_v_correct = AvgCounter("valid correct")

    avg_error_cost = AvgCounter("error cost")
    avg_reg_cost = AvgCounter("reg cost")
    for tid, tconfig in enumerate(training_config):
        #reset_sq(params)
        model_count = tconfig.get("model_offset", 0)
        print_time = tconfig.get("print_time", 500)
        negsamp_array = tconfig.get("negsamp",nconfig["s_vocab"])
        avg_t_cost.reset()
        avg_t_correct.reset()
        avg_t_total_correct.reset()
        avg_v_cost.reset()
        avg_t_correct.reset()

        avg_error_cost.reset()
        avg_reg_cost.reset()
        min_valid_cost = np.inf
        valid_wait = 0
        for iter in range(tconfig['iter']):
            tprofiler.start()

            j = 0
            # for line in read_input_file(train_file_idx, False):
            for case_x, case_y in gen_input_data(t_file_idx, s_window, pad_id, negsamp_array):

                result = t_networks(case_x, case_y)
                j += 1

                correct_ratio = get_correct_ratio(case_y, result[0])
                avg_t_cost.add(result[1])
                avg_t_correct.add(correct_ratio)
                avg_t_total_correct.add(correct_ratio)

                avg_error_cost.add(result[-2])
                avg_reg_cost.add(result[-1])
                if j % print_time == 0:
                    msg = "tid=%s iter=%s, j=%s, %s, %s" % (
                        tid, iter, j,
                        avg_t_cost.get_and_reset_str(),
                        avg_t_correct.get_and_reset_str()
                    )
                    my_print(msg)

                    msg2 = "%s, %s" % (
                        avg_error_cost.get_and_reset_str(),
                        avg_reg_cost.get_and_reset_str()
                    )
                    my_print(msg2)
            my_print("do validation...")

            for case_x, case_y in gen_input_data(v_file_idx, s_window, pad_id, negsamp_array, False):
                result = v_networks(case_x, case_y)
                cost = result[1]
                correct_ratio = get_correct_ratio(case_y, result[0])
                avg_v_cost.add(cost)
                avg_v_correct.add(correct_ratio)

            avg_v_cost_val = avg_v_cost.get_and_reset()
            avg_v_correct_val = avg_v_correct.get_and_reset()

            msg = "avg_correct=%s, val_correct=%s" % (avg_t_total_correct.get_and_reset_str(), avg_v_correct_val)

            if avg_v_correct_val > min_valid_cost:
                valid_wait = 0
                min_valid_cost = avg_v_correct_val
                model_name = tconfig["model_name"] % model_count
                model_count += 1
                write_model(params, model_name)
                msg += ", save_model=%s" % (model_name)
            else:
                valid_wait += 1
                msg += ", valid_wait=%s" % (valid_wait)
                if valid_wait > 30:
                    msg += ", early stop"
                    my_print(msg)
                    break

            # print "save_model"
            # msg = "avg_correct=%s, save_model=%s" % (avg_t_correct.get_and_reset_str(), model_name)
            my_print(msg)
            tprofiler.stop()
            my_print(tprofiler.get_period())


def main():
    dict_fname = "word_freq_dict.json"
    pre_model_fname = "model/model_autoenc_3_37.json"
    word_dict = read_json(dict_fname)
    #negsamp_array = load_negsamp_array(word_dict)

    nconfig = {
        "s_vocab": len(word_dict),
        "s_embed": 100,
        "s_window": 5,
        "s_hidden": 400,
        "s_hidden2": 400,
        "s_hidden3": 400,
        "p_lambda": 0.0001,
    }

    pre_model_params = read_json(pre_model_fname)
    t_idx_path = "./data/full/pku_t_label.txt"
    v_idx_path = "./data/full/pku_test_t_label.txt"

    training_config = [
        {"iter": 1200,
         "model_name": "model_softmax_4_%s.json",
         "print_time": 30,
         },
    ]
    t_networks, v_networks, params = build_networks(nconfig, pre_model_params)
    trainig(t_idx_path, v_idx_path, t_networks, v_networks, params, nconfig, training_config)


if __name__ == "__main__":
    main()
