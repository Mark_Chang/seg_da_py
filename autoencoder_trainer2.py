import theano.tensor as T
import theano
import numpy as np
import json
import math
import copy
from collections import OrderedDict
from util import read_file_line, read_json
import logging
from profiler import Profiler
from embedding_trainer import load_negsamp_array, my_print, weight_init, weight_init_val, weight_init_const, \
    adagrad_update, adadelta_updates, gen_input_data, write_model, reset_sq, trainig

np.random.seed(1)

logging.basicConfig(filename='out.log', format='%(asctime)s [%(levelname)s]:%(message)s', level=logging.INFO,
                    datefmt='%Y-%m-%d %I:%M:%S')


def build_networks(config, pre_model_params, init_params=None):
    # s_vocab = config["s_vocab"]
    s_embed = config["s_embed"]
    s_window = config["s_window"]
    s_hidden = config["s_hidden"]
    s_hidden2 = config["s_hidden2"]
    p_lambda = config.get("p_lambda", 10.)
    p_rho = 0.9
    p_eta = 0.1
    p_eps = 1.0  # will be reset

    wb = OrderedDict()
    # gdss = {}
    # deltass = {}
    if init_params:
        for wb_s_key in init_params['wb'].keys():
            wb[wb_s_key] = weight_init_val(init_params['wb'][wb_s_key])
    else:
        wb['w_embed'] = weight_init_val(pre_model_params['wb']['w_embed'])
        wb['w_hidden'] = weight_init_val(pre_model_params['wb']['w_hidden'])
        wb['b_hidden'] = weight_init_val(pre_model_params['wb']['b_hidden'])
        wb_shape = OrderedDict({
            # "w_embed": (s_vocab, s_embed),
            "w_hidden2": (s_hidden, s_hidden2),
            "b_hidden2": (s_hidden2,),
            "w_out": (s_hidden2, 1),
            "b_out": (1,)
        })
        for wb_s_key in wb_shape.keys():
            wb[wb_s_key] = weight_init(wb_shape[wb_s_key])

    x_in = T.imatrix()
    y_in = T.imatrix()

    emb_lookup = wb["w_embed"][x_in]
    hidden_input = T.reshape(emb_lookup, newshape=(x_in.shape[0], x_in.shape[1] * s_embed))
    hidden_result = T.tanh(T.dot(hidden_input, wb["w_hidden"]) + wb["b_hidden"])

    hidden2_result = T.tanh(T.dot(hidden_result, wb["w_hidden2"]) + wb["b_hidden2"])
    out_result = T.nnet.sigmoid(T.dot(hidden2_result, wb["w_out"]) + wb["b_out"])


    w_array = [T.sum(T.power(wb[k], 2)) for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    w_array_size = [wb[k].shape[0]*wb[k].shape[1] for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    w_sum = sum(w_array)/T.cast(sum(w_array_size),theano.config.floatX )


    error_cost = - T.mean((y_in * T.log(out_result) + (1 - y_in) * T.log(1 - out_result)))
    cost = error_cost + p_lambda * w_sum

    gd = OrderedDict()
    for wb_key in wb.keys():
        gd[wb_key] = T.grad(cost=cost, wrt=wb[wb_key])

    # wb_updates, gradients_sq, deltas_sq = adadelta_updates(wb, gd.values(), p_rho, p_eps, init_params)
    wb_updates, gradients_sq, deltas_sq = adagrad_update(wb, gd.values(), p_eta, p_eps, init_params)

    # func = theano.function(inputs=[x_in, y_in], outputs=[emb_lookup, hidden_input, hidden_result, out_result, cost],
    #                       updates=wb_updates, allow_input_downcast=True)


    #t_func = theano.function(inputs=[x_in, y_in], outputs=[out_result, cost], updates=wb_updates,
    #                         allow_input_downcast=True)

    t_func = theano.function(inputs=[x_in, y_in], outputs=[out_result, cost, error_cost, w_sum], updates=wb_updates,
                             allow_input_downcast=True)

    v_func = theano.function(inputs=[x_in, y_in], outputs=[out_result, cost], allow_input_downcast=True)

    params = OrderedDict()
    params['wb'] = wb
    params['gradients_sq'] = gradients_sq
    params['deltas_sq'] = deltas_sq
    return t_func, v_func, params


def main():
    dict_fname = "word_freq_dict.json"
    pre_model_fname = "model/model_autoenc_1_96.json"
    word_dict = read_json(dict_fname)
    #negsamp_array = load_negsamp_array(word_dict)

    nconfig = {
        "s_vocab": len(word_dict),
        "s_embed": 100,
        "s_window": 5,
        "s_hidden": 400,
        "s_hidden2": 400,
        "p_lambda": 0.00001,
    }

    pre_model_params = read_json(pre_model_fname)
    t_idx_path = "./data/converted/pku_t_idx.txt"
    v_idx_path = "./data/converted/pku_v_idx.txt"
     
    training_config = [
        {"iter": 1500,
         "model_name": "model_autoenc_2_%s.json",
         "print_time": 30,
         },
    ]
    t_networks, v_networks, params = build_networks(nconfig, pre_model_params)
    trainig(t_idx_path, v_idx_path, t_networks, v_networks, params, nconfig, training_config)


if __name__ == "__main__":
    main()
